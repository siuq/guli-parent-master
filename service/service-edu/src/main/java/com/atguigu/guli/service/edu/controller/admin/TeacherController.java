package com.atguigu.guli.service.edu.controller.admin;

import com.atguigu.guli.service.base.result.R;
import com.atguigu.guli.service.base.result.ResultCodeEnum;
import com.atguigu.guli.service.edu.entity.Teacher;
import com.atguigu.guli.service.edu.entity.query.TeacherQuery;
import com.atguigu.guli.service.edu.service.TeacherService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author sq
 * @since 2021-08-24
 */
@RestController
@Api (tags = "讲师管理")
@Slf4j
@CrossOrigin
@RequestMapping ("/admin/edu/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @ApiOperation ("所有讲师列表")
    @GetMapping ("list")
    public R listAll() {
        List<Teacher> list = teacherService.list();
        return R.ok().data("items", list).message("获取讲师列表成功");
    }

    @ApiOperation (value = "根据ID删除讲师", notes = "根据ID删除讲师")
    @DeleteMapping ("remove/{id}")
    public R removeById(@ApiParam (value = "讲师ID", required = true) @PathVariable String id) {
        boolean result = teacherService.removeById(id);
        return result ? R.ok().message("删除成功") : R.error().message("数据不存在");
    }

    @ApiOperation("根据id列表删除讲师")
    @DeleteMapping("batch-remove")
    public R removeRows(
            @ApiParam(value = "讲师id列表", required = true)
            @RequestBody List<String> idList){
        boolean result = teacherService.removeByIds(idList);
        if(result){
            return R.ok().message("删除成功");
        }else{
            return R.error().message("数据不存在");
        }
    }
    @ApiOperation ("查询指定id的讲师")
    @GetMapping ("getById/{id}")
    public R getById(@ApiParam (value = "讲师id", defaultValue = "1", required = true)
                     @PathVariable Long id) {
        try {
            Teacher teacher = teacherService.getById(id);
            if (teacher == null) {
                return R.error().message("数据不存在");
            }
            return R.ok().data("item", teacher);
        } catch (Exception e) {
            e.printStackTrace();
            return R.setResult(ResultCodeEnum.PARAM_ERROR);
        }
    }

    @ApiOperation ("更新讲师")
    @PutMapping ("update")
    public R updateById(@RequestBody Teacher teacher) {
        boolean result = teacherService.updateById(teacher);
        if (result) {
            return R.ok().message("修改成功");
        } else {
            return R.error().message("数据不存在");
        }
    }

    @ApiOperation ("新增讲师")
    @PostMapping ("save")
    public R save(@ApiParam (value = "讲师对象", required = true) @RequestBody Teacher teacher) {
        boolean result = teacherService.save(teacher);
        if (result) {
            return R.ok().message("添加成功");
        } else {
            return R.error().message("添加失败");
        }
    }

    @ApiOperation ("分页讲师列表")
    @GetMapping ("list/{page}/{limit}")
    public R listPage(@ApiParam (value = "当前页码", required = true) @PathVariable Long page,
                      @ApiParam (value = "每页记录数", required = true) @PathVariable Long limit,
                      @ApiParam (value = "讲师列表查询对象") TeacherQuery teacherQuery
    ) {
        IPage<Teacher> pageModel = teacherService.selectPage(page,limit,teacherQuery);
        return R.ok().data("pageModel", pageModel);
    }
    @ApiOperation("根据左关键字查询讲师名列表")
    @GetMapping("list/name/{key}")
    public R selectNameListByKey(
            @ApiParam(value = "查询关键字", required = true)
            @PathVariable String key){

        List<Map<String, Object>> nameList = teacherService.selectNameListByKey(key);

        return R.ok().data("nameList", nameList);
    }
}

