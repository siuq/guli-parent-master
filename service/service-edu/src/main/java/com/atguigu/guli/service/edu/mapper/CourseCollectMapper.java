package com.atguigu.guli.service.edu.mapper;

import com.atguigu.guli.service.edu.entity.CourseCollect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程收藏 Mapper 接口
 * </p>
 *
 * @author sq
 * @since 2021-08-24
 */
public interface CourseCollectMapper extends BaseMapper<CourseCollect> {

}
