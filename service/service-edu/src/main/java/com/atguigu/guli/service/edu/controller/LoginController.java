package com.atguigu.guli.service.edu.controller;

import com.atguigu.guli.service.base.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @Date 2021/8/26/0026 15:31
 */
@CrossOrigin
@RequestMapping("/user")
@RestController
public class LoginController {
    /**
     * 登录
     * @return
     */
    @PostMapping("login")
    public R login() {

        return R.ok().data("token","admin");
    }

    /**
     * 获取用户信息
     * @return
     */
    @GetMapping ("info")
    public R info() {

        return R.ok()
                .data("roles","[admin]")
                .data("name","admin")
                .data("avatar","https://oss.aliyuncs.com/aliyun_id_photo_bucket/default_handsome.jpg");
    }

    /**
     * 退出
     * @return
     */
    @PostMapping("logout")
    public R logout(){
        return R.ok();
    }
}
