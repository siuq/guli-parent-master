package com.atguigu.guli.service.edu.service;

import com.atguigu.guli.service.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author sq
 * @since 2021-08-24
 */
public interface CourseDescriptionService extends IService<CourseDescription> {

}
