package com.atguigu.guli.service.edu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author sq
 * @since 2021-08-24
 */
@RestController
@RequestMapping("/edu/chapter")
public class ChapterController {

}

