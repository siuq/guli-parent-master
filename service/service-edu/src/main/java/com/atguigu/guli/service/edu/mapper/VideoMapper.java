package com.atguigu.guli.service.edu.mapper;

import com.atguigu.guli.service.edu.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author sq
 * @since 2021-08-24
 */
public interface VideoMapper extends BaseMapper<Video> {

}
