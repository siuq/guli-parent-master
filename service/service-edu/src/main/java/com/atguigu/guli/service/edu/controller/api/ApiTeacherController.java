package com.atguigu.guli.service.edu.controller.api;

import com.atguigu.guli.service.base.result.R;
import com.atguigu.guli.service.base.result.ResultCodeEnum;
import com.atguigu.guli.service.edu.entity.Teacher;
import com.atguigu.guli.service.edu.service.TeacherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping ("/api/edu/teacher")
public class ApiTeacherController {
    @Autowired
    TeacherService teacherService;

    //查询所有的讲师
    /*
        springmvc中controller方法返回的数据会被视图对象解析：
            1、如果使用的是controller注解
                    返回的数据不是以 redircet:为前缀表示生成转发视图对象，到项目中以返回值去查找页面
                    返回的数据以redircet: 开始表示生成重定向视图，给浏览器响应重定向的报文

                    如果接口方法上使用@ResponseBody注解表示 返回的数据需要转为json或者字符串响应给前端
            2、如果使用的是@RestController注解表示接口方法的返回值转为json或者字符串响应
                    RestController = controller+response

     */
    @GetMapping ("list")
    public R getAll() {
//        log.debug("{}级别日志" , "debug");
//        log.info("{}级别日志" , "info");
//        log.warn("{}级别日志" , "warn");
//        log.error("{}级别日志" , "error");
        List<Teacher> teachers = teacherService.list();
        return R.ok().data("items", teachers);
    }

    //查询指定id的讲师
    @GetMapping ("getById/{id}")
    public R getById(@PathVariable String id) {
        try {
//            int i = 1/0;
            Teacher teacher = teacherService.getById(id);
            return R.ok().data("item", teacher);
        } catch (Exception e) {
            e.printStackTrace();
            return R.setResult(ResultCodeEnum.PARAM_ERROR);
        }
    }
}